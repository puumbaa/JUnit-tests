
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

public class TestByJunit {

    CommandCenter commandCenter = new CommandCenter();
    ArrayList<Spaceship> ships = new ArrayList<>();

    @AfterEach
    public void clearArr(){
        ships.clear();
    }

    @DisplayName("Тест на возврат корабля по имени и на то, что он первый из имеющихся ")
    @Test
    public void getShipByName_nameIsTrue() {
        ships.add(new Spaceship("someName", 10, 0, 0));
        ships.add(new Spaceship("foo", 10, 0, 0));

        Assertions.assertEquals(ships.get(0),commandCenter.getShipByName(ships,"someName"));

    }

    @DisplayName("Тест на отсутсвие корабля вызываемого по имени")
    @Test
    public void getShipByName_nameIsNotFound() {
        ships.add(new Spaceship("someName", 10, 0, 0));
        ships.add(new Spaceship("foo", 0, 0, 0));

        Assertions.assertNull(commandCenter.getShipByName(ships, "bar"));


    }

    @DisplayName("Тест на самый хорошо вооруженный корабль")
    @Test
    public void getMostPowerfulShip_isTrueShip(){
        ships.add(new Spaceship("first",10,0,0));
        ships.add(new Spaceship("second",10,0,0));

        Assertions.assertEquals(ships.get(0),commandCenter.getMostPowerfulShip(ships));


    }

    @DisplayName("Тест на отсутствие самого вооруженного корабля")
    @Test
    public void getMostPowerfulShip_shipIsNotFound(){
        ships.add(new Spaceship("first",0,0,0));
        ships.add(new Spaceship("second", 0,0,0));

        Assertions.assertNull(commandCenter.getMostPowerfulShip(ships));
    }

    @DisplayName("Тест на наличие кораблей с достаточно большим грузовым трюмом")
    @Test
    public void getAllShipsWithEnoughCargoSpace_isTrueShips(){
        ships.add(new Spaceship("first",0,10,0));
        ships.add(new Spaceship("second", 0,9,0));
        ArrayList<Spaceship> arrayList= new ArrayList<>();
        {
            arrayList.add(ships.get(0));
        }

        Assertions.assertEquals(arrayList, commandCenter.getAllShipsWithEnoughCargoSpace(ships,10));

    }


    @DisplayName("Тест на отсутствие кораблей с достаточно большим грузовым трюмом")
    @Test
    public void getAllShipsWithEnoughCargoSpace_isNotFound(){
        ships.add(new Spaceship("first",0,9,0));
        ships.add(new Spaceship("second", 0,9,0));

        Assertions.assertEquals(new ArrayList<Spaceship>(),
                commandCenter.getAllShipsWithEnoughCargoSpace(ships,10));
    }

    @DisplayName("Тест на наличие мирных кораблей ")
    @Test
    public void getAllCivilianShips_isTrueShips() {
        ships.add(new Spaceship("first",0,0,0));
        ships.add(new Spaceship("second", 1,0,0));

        List<Spaceship> exceptArr = new ArrayList<>();

        exceptArr.add(ships.get(0));


        Assertions.assertEquals(exceptArr,commandCenter.getAllCivilianShips(ships));
    }


    @DisplayName("Тест на отсутствие мирных кораблей")
    @Test
    public void getAllCivilianShips_isNotFound(){
        ships.add(new Spaceship("first",1,0,0));
        ships.add(new Spaceship("second", 1,0,0));
        Assertions.assertEquals(new ArrayList<Spaceship>(),commandCenter.getAllCivilianShips(ships));

    }
}
