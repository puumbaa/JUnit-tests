
import java.util.ArrayList;
import java.util.List;

public class SpaceshipFleetManagerTest {
    CommandCenter commandCenter = new CommandCenter();
    ArrayList<Spaceship> ships = new ArrayList<>();

    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest();

        System.out.println(spaceshipFleetManagerTest.getAllCivilianShips_isTrueShips());

    }

    public boolean getShipByName_nameIsTrue() {
        ships.add(new Spaceship("someName", 10, 0, 0));
        ships.add(new Spaceship("foo", 0, 0, 0));

        if (commandCenter.getShipByName(ships, "someName") == ships.get(0)) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }


    public boolean getShipByName_nameIsNotFound() {
        ships.add(new Spaceship("someName", 10, 0, 0));
        ships.add(new Spaceship("foo", 0, 0, 0));


        if (commandCenter.getShipByName(ships, "bar") == null) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }

    public boolean getMostPowerfulShip_isTrueShip(){
        ships.add(new Spaceship("first",10,0,0));
        ships.add(new Spaceship("second",10,0,0));

        if (commandCenter.getMostPowerfulShip(ships).getName().equals("first")) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }

    public boolean getMostPowerfulShip_shipIsNotFound(){
        ships.add(new Spaceship("first",0,0,0));
        ships.add(new Spaceship("second", 0,0,0));
        if (commandCenter.getMostPowerfulShip(ships)==null) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }

    public boolean getAllShipsWithEnoughCargoSpace_isTrueShips(){
        ships.add(new Spaceship("first",0,10,0));
        ships.add(new Spaceship("second", 0,9,0));
        List<Spaceship> temp = commandCenter.getAllShipsWithEnoughCargoSpace(ships,10);
        if(temp.get(0).getName().equals("first") && temp.size()==1){
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }

    public boolean getAllShipsWithEnoughCargoSpace_isNotFound(){
        ships.add(new Spaceship("first",0,9,0));
        ships.add(new Spaceship("second", 0,9,0));
        List<Spaceship> temp = commandCenter.getAllShipsWithEnoughCargoSpace(ships,10);
        if (temp.isEmpty()) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }

    public boolean getAllCivilianShips_isTrueShips() {
        ships.add(new Spaceship("first",0,0,0));
        ships.add(new Spaceship("second", 1,0,0));
        List<Spaceship> temp = commandCenter.getAllCivilianShips(ships);
        if(temp.get(0).getName().equals("first") && temp.size()==1){
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }
    public boolean getAllCivilianShips_isNotFound(){
        ships.add(new Spaceship("first",1,0,0));
        ships.add(new Spaceship("second", 1,0,0));
        List<Spaceship> temp = commandCenter.getAllCivilianShips(ships);
        if (temp.isEmpty()) {
            ships.clear();
            return true;
        }
        ships.clear();
        return false;
    }
}
