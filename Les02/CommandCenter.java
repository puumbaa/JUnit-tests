

import java.util.ArrayList;

public class CommandCenter implements SpaceshipFleetManager {
    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int max = 0;
        int index = 0;
        for(Spaceship x:ships){
            if(x.getFirePower()>max){
                max=x.getFirePower();
                index= ships.indexOf(x);
            }
        }
        if(max==0){
            return null;
        }
        return ships.get(index);
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for(Spaceship x: ships){
            if(x.getName().equals(name)) return x;
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> arr = new ArrayList<>();
        for(Spaceship x: ships){
            if (x.getCargoSpace()>=cargoSize) {
                arr.add(x);
            }
        }
        return arr;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> arr = new ArrayList<>();
        for(Spaceship x: ships){
            if (x.getFirePower()==0) {
                arr.add(x);
            }
        }
        return arr;
    }
// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).

}
